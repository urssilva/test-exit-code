import sys
import requests

if __name__ == '__main__':
    print('Begin test GET https://www.google.com')
    response = requests.get('https://www.google.com')
    print(f'HTTP Response: {response.status_code}')
    print('End test')
    sys.exit(20)
